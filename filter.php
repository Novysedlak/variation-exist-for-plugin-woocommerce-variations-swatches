<?php
if ( function_exists( 'wvs_variable_items_wrapper' ) ){
function hook_wvs_variable_items_wrapper( $data, $type, $options, $args, $saved_attribute ) {
global $post;
$style = '';
$id =  $post->ID;
$product_variation = new WC_Product_Variable( $id );
$product_variations = $product_variation->get_available_variations();
foreach($product_variations as $variation){
         $variation_id = $variation['variation_id'];
         $variation_obj = new WC_Product_variation($variation_id);
         $stock = $variation_obj->is_in_stock();
         $stock_value = $variation_obj->get_stock_quantity();
         if(!$stock or $stock_value == 0){ $style .= '.'.$options.'-variable-item-'.$variation['attributes']["attribute_".$args['attribute']].'{ -webkit-box-shadow: 0 0 0 2px rgba(255,0,0,.9) !important; box-shadow: 255 0 0 2px rgba(0,0,0,.9) !important; }'; }
}
$css = '<style>'.$style.'</style>';
return $css.$data;
}
add_filter('wvs_variable_items_wrapper','hook_wvs_variable_items_wrapper',10,5);
}
?>